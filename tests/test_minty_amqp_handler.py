# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from minty_amqp import consumer
from unittest import mock
from uuid import uuid4


class ExampleHandler(consumer.BaseHandler):
    @property
    def domain(self):
        # This seems to be the only way to get coverage of abstract properties..
        assert super().domain is None
        return "test_domain"

    @property
    def routing_keys(self):
        assert super().routing_keys is None
        return ["routing_1", "routing_2"]

    def handle(self, message):
        pass


def test_handler():
    mock_cqrs = mock.Mock()
    mock_cqrs.get_command_instance.return_value = "command instance"

    mock_event = mock.Mock()
    mock_event.context = "test_ctx"
    mock_event.user_uuid = "test_uuid"
    mock_event.user_info = minty.cqrs.UserInfo(
        user_uuid=uuid4(),
        permissions={"admin": True},
    )
    mock_event.correlation_id = "test_correlation"

    h = ExampleHandler(mock_cqrs)

    assert h.domain == "test_domain"
    assert h.routing_keys == ["routing_1", "routing_2"]

    ci = h.get_command_instance(mock_event)
    assert ci == "command instance"

    mock_cqrs.get_command_instance.assert_called_once_with(
        "test_correlation", "test_domain", "test_ctx", "test_uuid", mock.ANY
    )

    user_info_used = mock_cqrs.get_command_instance.call_args[0][4]

    assert isinstance(user_info_used, minty.cqrs.UserInfo)
    assert user_info_used.user_uuid == mock_event.user_info.user_uuid
    assert user_info_used.permissions == mock_event.user_info.permissions
